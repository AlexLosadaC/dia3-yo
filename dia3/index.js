'use strict';

require('dotenv').config();

const args = require('minimist') (process.argv.slice(2));
const translation = require('./lib/traslation.js')

const {suma, resta, multiplica, divide} = require('./lib/operations.js');


const { LANGUAGE } = process.env;
const txtTraslations = translation[LANGUAGE];
const validOperations=['suma', 'resta', 'multiplica', 'divide']


const {operation, valorA, valorB}= args;
if (!operation || !valorA || !valorB){
    console.log(txtTraslations.error1);
    process.exit();
} 

if(!validOperations.includes(operation)){
    console.log(txtTraslations.error2);
    process.exit();
}

let result;
switch (operation){
    case "suma": 
    result = suma(valorA, valorB);
    break;
    case "resta":
    result = resta(valorA, valorB);
        break;
    case "multiplica":
    result = multiplica(valorA, valorB);
        break;
    case "divide":
    result = divide(valorA, valorB);
        break;

}



console.log(txtTraslations.result + result);